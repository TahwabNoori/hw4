import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(HeroApp());

class HeroApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Animation HW',
      home: MainScreen(),
    );
  }
}
class PhotoHero extends StatelessWidget {
  const PhotoHero({ Key key, this.photo, this.onTap, this.width, this.height }) : super(key: key);

  final String photo;
  final VoidCallback onTap;
  final double width;
  final double height;

  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Hero(
        tag: photo,
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: onTap,
            child: Image.asset(
              photo,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
}

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget introText = Container(
      padding: EdgeInsets.all(12),
      child: Text(
        'I am by no means an expert surfer. I wouldn\'t even consider myself a good surfer. But I\'ve been surfing since February and have learned a lot about surfing in San Diego. I\'ve '
            'compiled a list of personal reviews of some beaches I\'ve been to as a guide to other beginners.',
        style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500, fontFamily: 'Roboto'),
        softWrap: true,
      ),
    );
    return Scaffold(
      appBar: AppBar(
        title: Text('Taco\'s Surf Guide'),
      ),
      body: ListView(
        padding: EdgeInsets.only( bottom: 8),
        children: [
           Image.asset('images/main-page.png', width: 600, height: 240, fit: BoxFit.cover,),
           introText,
           _beachButton("oside", 'images/oceanside.jpg', 'Oceanside Beach', context, 1),
           _beachButton('moonlight', 'images/moonlight-beach.jpg', 'Moonlight Beach', context, 2),
          _beachButton('la-jolla', 'images/lj-shores.jpg', 'La Jolla Shores', context, 3)
        ],
      )
    );
  }

  Container _beachButton (String tag, String image, String name, BuildContext context, int returnScreen){

    return Container(
      decoration: BoxDecoration(
        border: Border.all(width: 10, color: Colors.blue),
        borderRadius: const BorderRadius.all(const Radius.circular(20)),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
          child: PhotoHero(
                photo: image,
                width: 120,
                height: 80,
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (_) {
                  if(returnScreen == 1){
                    return OceanSideScreen();
                  }
                  else if(returnScreen == 2){
                    return MoonlightScreen();
                  }
                  else if(returnScreen == 3)
                  {
                    return LJScreen();
                  }
                  else{
                    return MainScreen();
                  }

                }));
              },
            )
          ),
          Container(
            padding: EdgeInsets.all(24),
            child: Text(name, style: TextStyle(fontSize: 24, fontWeight: FontWeight.w600),)
          ),
        ],
      ),
    );
  }
}

class OceanSideScreen extends StatelessWidget {
  @override

  Widget build(BuildContext context) {
    Widget location = Container(
        alignment: Alignment.center,
        padding: EdgeInsets.only(left: 75, top: 10, bottom: 8),
        child: Row(
        children: [
                  Icon(Icons.location_on, color: Colors.orangeAccent, size: 35,),
                  Text("Oceanside, California", style: TextStyle(fontWeight: FontWeight.w900, fontSize: 24,),)
                ],
            )
      );
    Widget summary = Container(
        padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
        child: Text(
          '\tThe Oceanside Pier is one of the most famous surf spots in the world. The beach break here has been a favorite of locals for decades. Conditions are usually decent with waves ranging'
              'from 3-5ft in size during the summer and picking up to 6-8ft in the winter. Water temperatures are typical for Northern San Diego and range from the low 60s to high 60s depending on the'
              ' season.\n\n '
              '\tLocalism isn\'t as severe as other breaks, but as always exercise good etiquette. The surfers get a piece of the beach all to themselves which is nice and it doesn\'t get as crowded'
              ' as other spots, though some kooks can get annoying. Also, be wary of the rip currents that form as a result of the pier. Not planning on catching a few waves? Walk into any one of the surf shops and meet the world famous shapers who make Oceanside the surfboard Mecca of the world. It is easily'
              ' my favorite spot I\'ve surfed so far.\n',
          style: TextStyle(fontSize: 18),
          softWrap: true,
        )
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('Oceanside Beach'),
      ),
      body: ListView(
        children: [
          PhotoHero(
            photo: 'images/oceanside.jpg',
            width: 600,
            height: 280,
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
          location,
          summary
        ],
      )
    );
  }
}



class MoonlightScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget location = Container(
        padding: EdgeInsets.only(left: 75, top: 10, bottom: 8),
        child: Row(
          children: [
            Icon(Icons.location_on, color: Colors.orangeAccent, size: 35,),
            Text("Encinitas, California", style: TextStyle(fontWeight: FontWeight.w900, fontSize: 24,),)
          ],
        )
    );
    Widget summary = Container(
        padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
        child: Text(
          'Encinitas is another famous, quintessential surf town. D-Street, officially named Moonlight Beach, is another famous beach break. A fast and powerful wave, Moonlight is frequented'
              ' by short-boarders and, as such, should be approached with caution. Size ranges from 2-5ft in the summer and 4-6ft in the winter. Water temperatures are the usual 60-70 degrees. Localism'
              ' can be more severe here when the conditions are fair so be fully aware of the lineup and etiquette. '
              '\n\n\tDon\'t bother showing up early for Moonlight as the waves are usually flat and the few waves that there are break super late right on the shore. Late afternoon to sunset is the best time to catch'
              ' good waves. Can you ride a long board here? Technically, yes. '
              ' But you\'re better off bringing a shortboard or a fish to this spot as people compete aggressively for waves.\n',
          style: TextStyle(fontSize: 18),
          softWrap: true,
        )
    );

    return Scaffold(
        appBar: AppBar(
          title: Text('Moonlight Beach'),
        ),
        body: ListView(
          children: [
            PhotoHero(
              photo: 'images/moonlight-beach.jpg',
              width: 600,
              height: 280,
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
            location,
            summary
          ],
        )
    );
  }
}

class LJScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget location = Container(
        padding: EdgeInsets.only(left: 75, top: 10, bottom: 8),
        child: Row(
          children: [
            Icon(Icons.location_on, color: Colors.orangeAccent, size: 35,),
            Text("San Diego, California", style: TextStyle(fontWeight: FontWeight.w900, fontSize: 24,),)
          ],
        )
    );
    Widget summary = Container(
        padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
        child: Text(
          'La Jolla Shores is the ultimate beginner break. Known worldwide as a popular tourist spot, LJ Shores have some of the easiest and most manageable waves around. Size ranges between '
              '2-5ft year-round with temperatures high 50s to mid 60s depending on the season. A very popular spot for surfers of all ages, skills, and styles, the Shores are best ridden on a longboard'
              ' or an egg-shaped surfboard. On certain days, conditions are ripe for a shorter board though not occasionally. \n\n\tDue to it\'s central location and beginner friendly environment, the beach is practically always packed'
              ' no matter the day, time, or conditions. I\'ve surfed in the middle of a rainstorm with about 30 other people around me. Localism is nonexistent and, as such, La Jolla Shores will always be the go to'
              ' beginner break for anyone just getting into the sport of surfing.\n',
          style: TextStyle(fontSize: 18),
          softWrap: true,
        )
    );

    return Scaffold(
        appBar: AppBar(
          title: Text('La Jolla Shores'),
        ),
        body: ListView(
          children: [
            PhotoHero(
              photo: 'images/lj-shores.jpg',
              width: 600,
              height: 280,
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
            location,
            summary
          ],
        )
    );
  }
}